
exports.up = function(knex, Promise) {
	return knex.schema.createTable('inquiry', table => {
		table.increments('id');
		table.string('name', 60).nullable();
		table.string('email', 60).nullable();
		table.timestamps('created_at').nullable();
	});  
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTableIfExists('inquiry');
};

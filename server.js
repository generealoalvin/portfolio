const express = require('express')
const path = require('path')
const argv = require('yargs').argv

const routes = require('./app/routes')

//initialized global variables
//NOTE: only use this when it is REALLY neccesary
global.vars = {};

//Port 7000 wil be the default port
const PORT = argv.port || 7000

//initialize express server
let app = express()

//expose the public folder
app.use(express.static(path.join(__dirname, '/public')))

//for allowing JSON to be sent in server when making an Ajax POST call
//file limit is 50mb
app.use(express.json({
    limit: '50mb'
}))

//register routes
routes.register(app)

//start the server
app.listen(PORT, () => {
    console.log(`Server start on port: ${PORT}`);
    
})
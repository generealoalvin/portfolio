import React from 'react'
import ReactDOM from "react-dom"

import ChatWindow from './ChatWindow/ChatWindow.jsx'

class ChatBubble extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show_chat_window: false,
        }

        this.chatBubbleClickHandle = this.chatBubbleClickHandle.bind(this)
    }

    chatBubbleClickHandle() {
        //show or hide the chat window
        let should_show_window = !this.state.show_chat_window;

        this.setState({
            show_chat_window: should_show_window
        })

    }

    render() {
        return (

            <div>
                <div className="chat-bubble">
                    <a href="javascript:void(0)" onClick={this.chatBubbleClickHandle}>
                        <img src="./images/bot_icon.png" alt="" />
                    </a>
                </div>

                <ChatWindow show={this.state.show_chat_window}/>

            </div>

        )
    }
}


ReactDOM.render(<ChatBubble />, document.getElementById('bot'))
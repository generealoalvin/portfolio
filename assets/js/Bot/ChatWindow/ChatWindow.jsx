import React from 'react'
import MessageArea from './sub-component/MessageArea/MessageArea.jsx'



class ChatWindow extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let classes_array = [
            'chat-window'
        ]
    
        classes_array.push(this.props.show ? 'show-window' : 'hide-window')
    
        let classes_string = _.join(classes_array, ' ')
    
        return (
            <div className={classes_string}>
                <table>
                    <tr>
                        <MessageArea />
                    </tr>
                </table>
            </div>
        )
    }
}

export default ChatWindow
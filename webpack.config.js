const path = require("path");
const webpack = require("webpack");
const webpack_rules = [];

const webpackOption = {
    entry: {
        //put here the files to compile
        //filename : path_location
        // sample: "./assets/js/sample.jsx"
        chat_bubble: "./assets/js/Bot/ChatBubble.jsx",
			inquire: "./assets/js/custom/inquire.js"
    },
    output: {
        path: path.join(__dirname, "/public/js"),
        filename: "[name].js",
    },
    module: {
        rules: webpack_rules
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            _: 'lodash',
		axios: 'axios'
		}),//
    ]
};
let babelLoader = {
    test: /\.(jsx|js)$/,
    exclude: /(node_modules|bower_components)/,
    use: {
        loader: "babel-loader",
        options: {
            presets: ["@babel/preset-env", "@babel/preset-react"]
        }
    }
};
webpack_rules.push(babelLoader);
module.exports = webpackOption;

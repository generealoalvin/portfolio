const path = require('path')
const InquireController = require('./Controllers/InquireController');
const register = app => {
    app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, '/Views/index.html'))
    })
	
	app.post('/inquire', (req, res) => {
		res.send(res);
	});

	app.post('/test', (req, res) => {
		let data = InquireController.saveInquiry(req.body);
		res.send(data);
		
	});
}


module.exports = {
    register,
}
